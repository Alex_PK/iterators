<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators;

/**
 * Class Iterator
 * @package Apk\Iterators
 *
 * Implements the basic iterator with all the adaptor and consumer traits.
 */
class Iterator extends BaseIterator implements Iterable
{
	use StaticTrait;
	use AdaptorTrait;
	use ConsumerTrait;
}

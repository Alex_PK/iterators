<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators\Adaptor;

use Apk\Iterators\Iterator;

/**
 * Class Filter
 * @package Apk\Iterators\Adaptor
 *
 * Filters the elements of the array, returning only those that match (return true) the given function
 */
class Filter extends Iterator
{
	protected $filterFunc = null;

	/**
	 * @param array|\ArrayIterator|\Traversable $iter
	 *      Iterator to attach the filtering function to
	 *
	 * @param \Closure                          $filterFunc
	 *      $filterFunc($element) : bool
	 *      This function will be called for each element in the iterator.
	 *      If it returns true, the element will go through.
	 *      If it returns false, the element will be ignored.
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($iter, $filterFunc)
	{
		if ( !is_callable($filterFunc) ) {
			throw new \InvalidArgumentException('Filter function must be callable');
		}

		parent::__construct($iter);
		$this->filterFunc = $filterFunc;
	}

	public function valid()
	{
		while ( parent::valid() && !call_user_func($this->filterFunc, parent::current()) ) {
			parent::next();
		}

		return parent::valid();
	}
}

<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators\Adaptor;

use Apk\Iterators\Iterator;

/**
 * Class Skip
 * @package Apk\Iterators\Adaptor
 *
 * Skips a certain number of elements at the beginning of the iterator
 */
class Skip extends Iterator
{
	protected $numSkip = 0;
	protected $stillToSkip = 0;

	/**
	 * @param array|\ArrayIterator|\Traversable $iter
	 *      The iterator to attach to.
	 *
	 * @param int                               $numSkip
	 *      Number of elements to skip (ignore) when processing the iterator.
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($iter, $numSkip)
	{
		if ( !is_int($numSkip) ) {
			throw new \InvalidArgumentException('Number of elements to skip must be integer');
		}

		parent::__construct($iter);
		$this->numSkip = $numSkip;
		$this->stillToSkip = $numSkip;
	}

	public function valid()
	{
		while ( $this->stillToSkip > 0 && parent::valid() ) {
			parent::current();   // workaroud for some lazy iterators not doing next() unless you do a current() (SplFileObject)
			parent::next();
			$this->stillToSkip--;
		}

		return parent::valid();
	}

	public function rewind()
	{
		$this->stillToSkip = $this->numSkip;
		parent::rewind();
	}
}

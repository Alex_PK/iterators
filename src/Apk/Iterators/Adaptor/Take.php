<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators\Adaptor;

use Apk\Iterators\Iterator;

/**
 * Class Take
 * @package Apk\Iterators\Adaptor
 *
 * Takes up to a certain number of elements from the iterator, then ends.
 */
class Take extends Iterator
{
	protected $numTake = 0;
	protected $stillToTake = 0;

	/**
	 * @param array|\ArrayIterator|\Traversable $iter
	 *      The iterator to attach to
	 *
	 * @param int                               $numTake
	 *      Number of elements to take from the iterator
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($iter, $numTake)
	{
		if ( !is_int($numTake) ) {
			throw new \InvalidArgumentException('Number of elements to take must be integer');
		}

		parent::__construct($iter);
		$this->numTake = $numTake;
		$this->stillToTake = $numTake;
	}

	public function valid()
	{
		if ( parent::valid() && $this->stillToTake > 0 ) {
			$this->stillToTake--;

			return parent::valid();
		}

		return false;
	}

	public function rewind()
	{
		$this->stillToTake = $this->numTake;
		parent::rewind();
	}
}

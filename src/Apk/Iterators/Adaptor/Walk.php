<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators\Adaptor;

use Apk\Iterators\Iterator;

/**
 * Class Walk
 * @package Apk\Iterators\Adaptor
 *
 * Executes the function on every element of the array
 */
class Walk extends Iterator
{
	protected $mapFunc = null;

	/**
	 * @param array|\ArrayIterator|\Traversable $iter
	 *      The iterator to attach to.
	 *
	 * @param callable|\Closure                 $mapFunc
	 *      $walkFunc($element) -> $newElement
	 *      The function will be called on every element and can return the element itself, a modified version of it,
	 *      the result of any calculations or anything else that will be considered the new element in the next
	 *      step of the computation.
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($iter, $mapFunc)
	{
		if ( !is_callable($mapFunc) ) {
			throw new \InvalidArgumentException('Filter function must be callable');
		}

		parent::__construct($iter);
		$this->mapFunc = $mapFunc;
	}

	public function current()
	{
		return call_user_func($this->mapFunc, parent::current());
	}
}

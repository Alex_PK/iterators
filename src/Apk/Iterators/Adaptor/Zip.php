<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators\Adaptor;

use Apk\Iterators\Generator\ArrayIterator;
use Apk\Iterators\Iterator;

/**
 * Class Zip
 * @package Apk\Iterators\Adaptor
 *
 * Merges several iterators, returning at each step an array of elements, one from each iterator.
 * The process will stop when the shortest iterator is exhausted.
 */
class Zip extends Iterator
{
	/** @var  \Iterator[] $iterators */
	protected $iterators;

	protected $index = 0;

	/**
	 * Zip constructor.
	 *
	 * @param Iterator ...$args
	 *      Takes a list of iterators to merge
	 */
	public function __construct()
	{
		foreach (func_get_args() as $arg) {
			if (is_array($arg)) {
				$arg = new ArrayIterator($arg);

			} elseif ( ! $arg instanceof \Iterator) {
				throw new \InvalidArgumentException('Zip arguments must all be Iterators');
			}

			$this->iterators[] = $arg;
		}
	}

	public function current()
	{
		$result = [];
		foreach ($this->iterators as $iter) {
			$result[] = $iter->current();
		}
		return $result;
	}

	public function next()
	{
		foreach ($this->iterators as $iter) {
			$iter->next();
		}
		$this->index++;
	}

	public function key()
	{
		return $this->index;
	}

	public function valid()
	{
		$valid = true;
		foreach ($this->iterators as $iter) {
			$valid &= $iter->valid();
		}
		return $valid;
	}

	public function rewind()
	{
		foreach ($this->iterators as $iter) {
			$iter->rewind();
		}
		$this->index = 0;
	}
}

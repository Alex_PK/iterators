<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators\Generator;

use Apk\Iterators\ConsumerTrait;
use Apk\Iterators\AdaptorTrait;
use Apk\Iterators\Iterable;

/**
 * Class ArrayIterator
 * @package Apk\Iterators\Consumer
 *
 * An iterator acting like an array, allowing random access to elements.
 *
 * This works both as a Generator and a Consumer
 */
class ArrayIterator extends \ArrayIterator implements Iterable
{
	use ConsumerTrait;
	use AdaptorTrait;

	/**
	 * Creates a PHP array out of the values in the iterator
	 *
	 * @return array
	 */
	public function toArray()
	{
		return $this->getArrayCopy();
	}
}

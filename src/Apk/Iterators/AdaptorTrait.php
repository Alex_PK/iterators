<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators;

use Apk\Iterators\Adaptor\Filter;
use Apk\Iterators\Adaptor\Map;
use Apk\Iterators\Adaptor\Skip;
use Apk\Iterators\Adaptor\Take;
use Apk\Iterators\Adaptor\Walk;
use Apk\Iterators\Adaptor\Zip;

/**
 * Class AdaptorTrait
 * @package Apk\Iterators
 *
 * This trait implements all the library adaptors, to be attached to an iterator class
 */
trait AdaptorTrait		// implements Adaptable
{
	/**
	 * Maps every element in the iterator to a tuple (index, element)
	 *
	 * @param callable|\Closure $mapFunc
	 *      $mapFunc($element) : [$index, $newElement]
	 *      This function will be called with every element in turn. It must return a 2-element array.
	 *      The first element of the array will be the index to which the element will be mapped to.
	 *      The second is the element to be mapped. Could be the original element, its transformation or anything else.
	 *
	 * @return Map      Iterator that will automatically call the callable on every element
	 */
	public function map($mapFunc)
	{
		return new Map($this, $mapFunc);
	}

	/**
	 * Filters the elements of the array, returning only those that match (return true) the given function
	 *
	 * @param callable|\Closure $filterFunc
	 *      $filterFunc($element) : bool
	 *      This function will be called for each element in the iterator.
	 *      If it returns true, the element will go through.
	 *      If it returns false, the element will be ignored.
	 *
	 * @return Filter   Iterator thet will only return elements for which the function returns true
	 */
	public function filter($filterFunc = null)
	{
		return new Filter($this, $filterFunc);
	}

	/**
	 * Executes the function on every element of the array
	 *
	 * @param \Closure $walkFunc
	 *      $walkFunc($element) -> $newElement
	 *      The function will be called on every element and can return the element itself, a modified version of it,
	 *      the result of any calculations or anything else that will be considered the new element in the next
	 *      step of the computation.
	 *
	 * @return Walk     Iterator calling the function on every element
	 */
	public function walk($walkFunc = null)
	{
		return new Walk($this, $walkFunc);
	}

	/**
	 * Skips a certain number of elements at the beginning of the iterator
	 *
	 * @param int $numSkip
	 *      Number of elements to skip (ignore) when processing the iterator.
	 *
	 * @return Skip     Iterator skipping its first $numSkip elements while processing
	 */
	public function skip($numSkip = 0)
	{
		return new Skip($this, $numSkip);
	}

	/**
	 * Takes up to a certain number of elements from the iterator, then ends.
	 *
	 * @param int $numTake
	 *      Number of elements to take from the iterator
	 *
	 * @return Take     Iterator that will only pass $numTake elements through
	 */
	public function take($numTake = 0)
	{
		return new Take($this, $numTake);
	}

	/**
	 * Merges several iterators, returning at each step an array of elements, one from each iterator.
	 * The process will stop when the shortest iterator is exhausted.
	 *
	 * @param Iterator ...$args
	 *      Takes a list of iterators to merge
	 *
	 * @return Zip      Iterator that will return an array of other iterators elements at each step
	 */
	public function zip()
	{
		$iterators = [$this];
		foreach (func_get_args() as $iter) {
			$iterators[] = $iter;
		}

		$newClass = new \ReflectionClass('Apk\Iterators\Adaptor\Zip');
		return $newClass->newInstanceArgs($iterators);
	}
}

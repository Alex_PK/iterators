<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators;

trait StaticTrait
{
	/**
	 * Allows to create and immediately chain functions on the created object:
	 *
	 * Iterator::create()->someFunction()...
	 *
	 * In newer versions of PHP you can do the same with
	 * (new Iterator())->someFunction()...
	 *
	 * @param mixed ...$params
	 *
	 * @return Iterator
	 */
	static public function create($params)
	{
		$newClass = new \ReflectionClass(get_called_class());
		return $newClass->newInstanceArgs(func_get_args());
	}

}

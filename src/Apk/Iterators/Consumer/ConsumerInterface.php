<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015-2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators\Consumer;

/**
 * Interface ConsumerInterface
 * @package Apk\Iterators\Consumer
 *
 * Allow to implement a consumer suitable for the toConsumer() function.
 */
interface ConsumerInterface
{
	/**
	 * Will be executed before the iteration begins
	 */
	public function open();

	/**
	 * Will be executed at each step of the iteration
	 *
	 * @param mixed $data
	 *      The current iteration step data
	 *
	 * @param mixed|null $key
	 *      The key associated with the current item, if available. Null otherwise.
	 */
	public function write($data, $key = null);

	/**
	 * Will be called at the end of the iteration
	 */
	public function close();
}

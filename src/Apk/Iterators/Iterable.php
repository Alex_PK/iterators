<?php
/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators;

interface Iterable extends Adaptable, Consumable
{
}

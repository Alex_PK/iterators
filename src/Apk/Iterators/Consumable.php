<?php
/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2016 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace Apk\Iterators;

use Apk\Iterators\Adaptor\ConsumerAdaptor;
use Apk\Iterators\Generator\ArrayIterator;
use Apk\Iterators\Consumer\ConsumerInterface;

interface Consumable
{
	/**
	 * Counts the number of elements in the iterator by going through all of them
	 *
	 * @return int      Number of elements in the iterator
	 */
	public function count();

	/**
	 * Calculates the minimum among the values in the iterator
	 *
	 * @param \Closure|null $compareFunc
	 *      $compareFunc(mixed $element, mixed|null $previousMin) : -1|0|1
	 *      This function should return -1 if $element < $previousMin, 0 if they are equal, +1 if greater.
	 *      If the values are already numeric or string, the function is optional and normal < and > will be used.
	 *
	 * @return mixed    The minimum value or element at the end of the calculation
	 *
	 * @throws \InvalidArgumentException
	 */
	public function min($compareFunc = null);

	/**
	 * Calculates the maximum among the values in the iterator
	 *
	 * @param \Closure|null $compareFunc
	 *      $compareFunc(mixed $element, mixed|null $previousMax) : -1|0|1
	 *      This function should return -1 if $element < $previousMax, 0 if they are equal, +1 if greater
	 *      If the values are already numeric or string, the function is optional and normal < and > will be used.
	 *
	 * @return mixed    The maximum value or element at the end of the calculation
	 *
	 * @throws \InvalidArgumentException
	 */
	public function max($compareFunc = null);

	/**
	 * Calculates the average among the iterator elements.
	 *
	 * @param \Closure|null $valueFunc
	 *      $valueFunc(mixed $element) : float|int
	 *      The function should return a numeric value for the given element of the iterator.
	 *      The average will be calculated on these numerical values.
	 *      If the elements are already numeric, the function is optional
	 *
	 * @return float|int    The calculated average value
	 */
	public function avg($valueFunc = null);

	/**
	 * Returns the first element in the iterator for which the function returns true.
	 *
	 * @param \Closure $findFunc
	 *      $findFunc(mixed $element) : bool
	 *      Returns true for the element that needs to be found.
	 *
	 * @return mixed    Returns the first element for which the function is true
	 */
	public function find($findFunc);

	/**
	 * "Folds" an iterator to a single value
	 *
	 * @param mixed    $base    The basic value to start from
	 * @param \Closure $foldFunc
	 *      $foldFunc(mixed $element, mixed $previousFold) : mixed
	 *      Calculates the new folded value based on the element and the previously folded value.
	 *
	 * @return mixed    The result of the folding
	 */
	public function fold($base, $foldFunc);

	/**
	 * Reduces an iterator by calculating a fold for each index.
	 * Normal PHP arrays do not allow multiple elements with the same index, so most of the times this function will
	 * be chained to a map() result (iterators can have the same index for multiple values).
	 *
	 * @param \Closure           $reduceFunc
	 *      $reduceFunc(mixed $element, mixed|null $previousResult, mixed $index) : mixed
	 *      Receives the current element and the previous collector for the element index and should
	 *      return the new value associated with the index.
	 *
	 * @param \ArrayAccess|array|null $resultObject
	 *      The resulting array-capable object to collect the results.
	 *      If null, an ArrayIterator will be used
	 *
	 * @return ArrayIterator|array|\ArrayAccess
	 */
	public function reduce($reduceFunc, $resultObject = null);

	/**
	 * Processes all the adaptors and consumers and returns no value.
	 * Useful when the consumers already do something with the data (save them to disk, send to net, etc.)
	 *
	 * @return void
	 */
	public function run();

	/**
	 * Collects the results of the iterator processing into a PHP array
	 *
	 * @return array
	 */
	public function toArray();

	/**
	 * Collects the results of the iterator processing into an object with an array-like interface
	 *
	 * @param null|\ArrayAccess $targetObject
	 *      The object to use as a collector. If null, an ArrayIterator will be created
	 *
	 * @return array|\ArrayAccess   The collector for the results of the iterator processing
	 */
	public function collect($targetObject = null);

	/**
	 * Appends a consumer object to the processing queue.
	 * This is more flexible than using a "simple" ArrayAccess object or an array, for example to open and close
	 *  a file or a DB connection before and after processing.
	 *
	 * @param ConsumerInterface|object $consumer
	 *
	 * @return ConsumerAdaptor  An iterator that will call the consumer during the data processing
	 */
	public function toConsumer($consumer);

}

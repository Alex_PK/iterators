<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Integration;

use Apk\Iterators\Iterator;

class TakeTest extends \PHPUnit_Framework_TestCase
{
	function testBasic()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take(3);

		$this->assertEquals(
			[4, 7, 2],
			$filtered->toArray()
		);
	}

	function testMultipleCalls()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take(3);

		$this->assertEquals(
			[4, 7, 2],
			$filtered->toArray()
		);

		$this->assertEquals(
			[4, 7, 2],
			$filtered->toArray()
		);
	}

	function testMin()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take(3);

		$this->assertEquals(
			2,
			$filtered->min()
		);
	}

	function testMax()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take(3);

		$this->assertEquals(
			7,
			$filtered->max()
		);
	}

	function testAvg()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take(3);

		$this->assertEquals(
			4.3333,
			$filtered->avg(),
			'',
			0.0001
		);
	}

	function testFind()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take(3);

		$this->assertEquals(
			7,
			$filtered->find(
				function ($el) {
					return $el > 5;
				}
			)
		);
	}

	function testFold()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take(3);

		$this->assertEquals(
			4 + 7 + 2,
			$filtered->fold(
				0,
				function ($el, $temp) {
					return $temp + $el;
				}
			)
		);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidFunction()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$filtered = $iter->take('not a number');

		$this->assertEquals(
			[4, 7, 2, 9, 5],
			$filtered->toArray()
		);
	}
}

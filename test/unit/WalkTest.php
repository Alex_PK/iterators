<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Adaptor\Walk;

class WalkTest extends \PHPUnit_Framework_TestCase
{
	function testBasic()
	{
		$result = 0;

		$filtered = new Walk([4, 7, 2, 9, 5], function($el) use (&$result) {
			$result += $el;
		});
		$filtered->run();

		$this->assertEquals(4+7+2+9+5, $result);
	}

	function testBasicStatic()
	{
		$result = 0;

		Walk::create([4, 7, 2, 9, 5], function($el) use (&$result) {
			$result += $el;
		})->run();

		$this->assertEquals(4+7+2+9+5, $result);
	}

}

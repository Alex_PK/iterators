<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Adaptor\Skip;

class SkipTest extends \PHPUnit_Framework_TestCase
{
	function testBasic()
	{
		$filtered = new Skip([4, 7, 2, 9, 5], 3);

		$this->assertEquals(
			[9, 5],
			$filtered->toArray()
		);
	}

	function testBasicStatic()
	{
		$filtered = Skip::create([4, 7, 2, 9, 5], 3);

		$this->assertEquals(
			[9, 5],
			$filtered->toArray()
		);
	}

	function testMin()
	{
		$filtered = new Skip([4, 7, 2, 9, 5], 3);

		$this->assertEquals(
			5,
			$filtered->min()
		);
	}

	function testMax()
	{
		$filtered = new Skip([4, 7, 2, 9, 5], 3);

		$this->assertEquals(
			9,
			$filtered->max()
		);
	}

	function testAvg()
	{
		$filtered = new Skip([4, 7, 2, 9, 5], 3);

		$this->assertEquals(
			7,
			$filtered->avg(),
			'',
			0.0001
		);
	}

	function testFind()
	{
		$filtered = new Skip([4, 7, 2, 9, 5], 3);

		$this->assertEquals(
			9,
			$filtered->find(
				function ($el) {
					return $el > 6;
				}
			)
		);
	}

	function testFold()
	{
		$filtered = new Skip([4, 7, 2, 9, 5], 3);

		$this->assertEquals(
			9 + 5,
			$filtered->fold(
				0,
				function ($el, $temp) {
					return $temp + $el;
				}
			)
		);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidFunction()
	{
		$filtered = new Skip([4, 7, 2, 9, 5], 'not a number');

		$this->assertEquals(
			[4, 7, 2, 9, 5],
			$filtered->toArray()
		);
	}
}

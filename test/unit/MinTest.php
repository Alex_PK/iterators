<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Iterator;

class MinTest extends \PHPUnit_Framework_TestCase
{
	function testInt()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);

		$this->assertEquals(2, $iter->min());

		$this->assertEquals(
			2,
			$iter->min(
				function ($el, $prev) {
					return ($el < $prev ? -1 : 1);
				}
			)
		);
	}

	function testObject()
	{
		$iter = new Iterator(
			[
				[
					'name' => 'Alex',
				    'age' => 4
				],
				[
					'name' => 'Barbara',
					'age' => 7
				],
				[
					'name' => 'Charlie',
					'age' => 2
				],
				[
					'name' => 'David',
					'age' => 9
				],
				[
					'name' => 'Ellen',
					'age' => 5
				]
			]
		);

		$resultName = $iter->min(function($el, $prev) { return ($el['name'] < $prev['name'] ? -1 : 1); } );
		$resultAge = $iter->min(function($el, $prev) { return ($el['age'] < $prev['age'] ? -1 : 1); } );

		$this->assertEquals('Alex', $resultName['name']);
		$this->assertEquals(4, $resultName['age']);

		$this->assertEquals('Charlie', $resultAge['name']);
		$this->assertEquals(2, $resultAge['age']);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidFunction()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->min('invalid');
	}
}

<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Iterator;

class BasicTest extends \PHPUnit_Framework_TestCase
{
	function testCreateStatic()
	{
		$iter = Iterator::create([1, 2, 3, 4, 5, 6]);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(1+2+3+4+5+6, $result);
	}

	function testCreateStaticAndFilter()
	{
		$iter = Iterator::create([1, 2, 3, 4, 5, 6])
			->filter(function($el) {
				return $el > 3;
			})
		;

		$this->assertEquals(4, $iter->min());
	}

}

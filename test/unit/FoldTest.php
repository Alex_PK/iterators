<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Iterator;

class FoldTest extends \PHPUnit_Framework_TestCase
{
	function testFold()
	{
		$iter = new Iterator([4,7,2,9,5]);

		$this->assertEquals(4+7+2+9+5, $iter->fold(0, function($el, $partial) { return $partial + $el; }));
		$this->assertEquals(4*7*2*9*5, $iter->fold(1, function($el, $partial) { return $partial * $el; }));
		$this->assertEquals(0, $iter->fold(0, function($el, $partial) { return $partial * $el; }));
		$this->assertEquals('47295', $iter->fold('', function($el, $partial) { return $partial . $el; }));
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidFunction()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->fold(0, 'invalid');
	}

}

<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Adaptor\Zip;

class ZipTest extends \PHPUnit_Framework_TestCase
{
	function testBasic()
	{
		$a1 = [4, 7, 2, 9, 5];
		$a2 = ['a', 'e', 'o', 'u', 'i', 'q'];

		$filtered = new Zip($a1, $a2);
		$result = $filtered->toArray();

		$this->assertEquals(5, count($result));
		$this->assertEquals([4, 'a'], $result[0]);
		$this->assertEquals([7, 'e'], $result[1]);
		$this->assertEquals([2, 'o'], $result[2]);
		$this->assertEquals([9, 'u'], $result[3]);
		$this->assertEquals([5, 'i'], $result[4]);
	}

}

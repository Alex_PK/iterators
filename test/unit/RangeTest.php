<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Generator\Range;

class RangeTest extends \PHPUnit_Framework_TestCase
{
	function testStaticCreate()
	{
		$iter = Range::create(0, 10);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(1+2+3+4+5+6+7+8+9+10, $result);

	}

	function testInt()
	{
		$iter = new Range(0, 10);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(1+2+3+4+5+6+7+8+9+10, $result);
	}

	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testIntWrongStep()
	{
		$iter = new Range(0, -2);
	}
	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testIntWrongStepReverse()
	{
		$iter = new Range(0, 2, -1);
	}


	function testIntStartNotZero()
	{
		$iter = new Range(5, 10);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(5+6+7+8+9+10, $result);
	}

	function testIntNegative()
	{
		$iter = new Range(-10, 0);
		$result = 0;
		foreach ($iter as $num) {
			$result += $num;
		}

		$this->assertEquals(-1-2-3-4-5-6-7-8-9-10, $result);
	}

	function testFloat()
	{
		$iter = new Range(0.0, 10.0, 0.2);
		$steps = 0;
		foreach ($iter as $num) {
			$steps++;
		}

		$this->assertEquals(51, $steps);
	}

	function testFloatStartNotZero()
	{
		$iter = new Range(2.0, 10.0, 0.2);
		$steps = 0;
		foreach ($iter as $num) {
			$steps++;
		}

		$this->assertEquals(41, $steps);
	}

	function testFloatNegative()
	{
		$iter = new Range(-10.0, 0.0, 0.2);
		$steps = 0;
		foreach ($iter as $num) {
			$steps++;
		}

		$this->assertEquals(51, $steps);
	}

	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testFloatWrongStep()
	{
		$iter = new Range(0.0, 2.0, -0.1);
	}

	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testFloatWrongStepReverse()
	{
		$iter = new Range(0.0, -2.0, 0.1);
	}


}

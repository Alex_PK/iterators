<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Iterator;

class FindTest extends \PHPUnit_Framework_TestCase
{
	function testFind()
	{
		$iter = new Iterator([4,7,2,9,5]);

		$this->assertEquals(7, $iter->find(function($el) { return $el == 7; }));
		$this->assertEquals(4, $iter->find(function($el) { return $el == 4; }));
		$this->assertEquals(5, $iter->find(function($el) { return $el == 5; }));
	}

	function testObject()
	{
		$iter = new Iterator(
			[
				[
					'name' => 'Alex',
					'age' => 4
				],
				[
					'name' => 'Barbara',
					'age' => 7
				],
				[
					'name' => 'Charlie',
					'age' => 2
				],
				[
					'name' => 'David',
					'age' => 9
				],
				[
					'name' => 'Ellen',
					'age' => 5
				]
			]
		);

		$result = $iter->find(function($el) { return ($el['age'] == 2); } );

		$this->assertEquals('Charlie', $result['name']);
		$this->assertEquals(2, $result['age']);
	}

	function testObjectClosure()
	{
		$iter = new Iterator(
			[
				[
					'name' => 'Alex',
					'age' => 4
				],
				[
					'name' => 'Barbara',
					'age' => 7
				],
				[
					'name' => 'Charlie',
					'age' => 2
				],
				[
					'name' => 'David',
					'age' => 9
				],
				[
					'name' => 'Ellen',
					'age' => 5
				]
			]
		);

		$age = 9;

		$result = $iter->find(function($el) use ($age) { return ($el['age'] == $age); } );

		$this->assertEquals('David', $result['name']);
		$this->assertEquals(9, $result['age']);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidFunction()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->find('invalid');
	}

}

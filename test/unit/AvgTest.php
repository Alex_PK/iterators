<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Iterator;

class AvgTest extends \PHPUnit_Framework_TestCase
{
	function testAvgInt()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);

		$this->assertEquals(5.4, $iter->avg(), '', 0.0001);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	function testInvalidFunction()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->avg('invalid');
	}

	/**
	 * @expectedException \UnexpectedValueException
	 */
	function testInvalidFunctionReturnValue()
	{
		$iter = new Iterator([4, 7, 2, 9, 5]);
		$iter->avg(
			function ($el) {
				return 'a' . $el;
			}
		);
	}

}

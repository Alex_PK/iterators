<?php

/**
 * This file is part of apk/iterators
 *
 * (c) Copyright 2015 Alessandro Pellizzari <alex@amiran.it>
 *
 * Distributed under the BSD license.
 * For the full copyright and license informations, see the LICENSE file distributed with this source code.
 */

namespace IteratorsTests\Unit;

use Apk\Iterators\Generator\StringWords;

class StringWordsTest extends \PHPUnit_Framework_TestCase
{
	function testEmptyString()
	{
		$iter = StringWords::create('');
		$result = [];
		foreach ($iter as $word) {
			$result[] = $word;
		}

		$this->assertEquals(0, count($result));
	}

	function testOneWordString()
	{
		$iter = StringWords::create('Single');
		$result = [];
		foreach ($iter as $word) {
			$result[] = $word;
		}

		$this->assertEquals(1, count($result));
		$this->assertEquals('Single', $result[0]);
	}

	function testTwoWordString()
	{
		$iter = StringWords::create('Two words');
		$result = [];
		foreach ($iter as $word) {
			$result[] = $word;
		}

		$this->assertEquals(2, count($result));
		$this->assertEquals('Two', $result[0]);
		$this->assertEquals('words', $result[1]);
	}

	function testIndexesString()
	{
		$iter = StringWords::create('Two words');
		$indexes = [];
		$result = [];
		foreach ($iter as $idx => $word) {
			$indexes[] = $idx;
			$result[] = $word;
		}

		$this->assertEquals(2, count($result));
		$this->assertEquals('Two', $result[0]);
		$this->assertEquals('words', $result[1]);

		$this->assertEquals(2, count($indexes));
		$this->assertEquals(0, $indexes[0]);
		$this->assertEquals(4, $indexes[1]);

	}

	function testMultibyteString()
	{
		$iter = StringWords::create('こんい知は　ともだち');
		$result = [];
		foreach ($iter as $word) {
			$result[] = $word;
		}

		$this->assertEquals(2, count($result));
		$this->assertEquals('こんい知は', $result[0]);
		$this->assertEquals('ともだち', $result[1]);
	}

	function testMultilineString()
	{
		$iter = StringWords::create("More words\non different lines\rwith different\r\nline endings");
		$result = [];
		foreach ($iter as $word) {
			$result[] = $word;
		}

		$this->assertEquals(9, count($result));
		$this->assertEquals('More', $result[0]);
		$this->assertEquals('words', $result[1]);
		$this->assertEquals('on', $result[2]);
		$this->assertEquals('different', $result[3]);
		$this->assertEquals('lines', $result[4]);
		$this->assertEquals('with', $result[5]);
		$this->assertEquals('different', $result[6]);
		$this->assertEquals('line', $result[7]);
		$this->assertEquals('endings', $result[8]);
	}


}
